from django.shortcuts import render
from django.http import JsonResponse
import base64
import os
import requests
from io import BytesIO
import json
import cv2
import numpy as np
from PIL import Image
from app.barcode import barCode
from django.views.decorators.csrf import csrf_exempt
import matplotlib.pyplot as plt
import matplotlib as mpl
from app.pneumonia_pytorch_localization import predict_pneumonia as p
from app.mask_detection import tensorflow_infer as infer


def validate_files(files):
    for file in files:
        image_extensions = ['ras', 'xwd', 'bmp', 'jpe', 'jpg', 'jpeg', 'xpm', 'ief', 'pbm', 'tif', 'gif', 'ppm', 'xbm',
                            'tiff', 'rgb', 'pgm', 'png', 'pnm']
        ext = file.name.split('.')
        ext = ext[len(ext) - 1]
        if ext.lower() not in image_extensions:
            return False
    return True


def img_to_results(file):

    classes = ['Normal', 'Pneumonia']
    img = p.image_loader(file)
    img, class_activation, pred = p.predict_img(img)
    pred = classes[pred.item()]
    try:
        name = file.name.split("/")
    except AttributeError:
        name = file.split("/")
    name = name[len(name) - 1].split(".")[0]

    prediction = {
        'name': name,
        'pred': pred,
    }

    mpl.use('Agg')
    plt.ioff()
    plt.axis('off')
    plt.imshow(class_activation, cmap='jet', alpha=1, aspect='equal')
    plt.imshow(img, alpha=0.55, aspect='equal')
    plt.title(name + ' - ' + pred)
    plt.tight_layout()
    fig = plt.gcf()
    # canvas = FigureCanvas(fig)
    buf = BytesIO()
    fig.savefig(buf, format='png', bbox_inches='tight')
    buf.seek(0)
    string = base64.b64encode(buf.read())

    # img = Image.fromarray((img * 255).astype(np.uint8))
    # cm = plt.get_cmap('jet')
    # colored_image = cm(class_activation)
    # class_activation = Image.fromarray((colored_image[:, :, :3] * 255).astype(np.uint8))
    #
    # img = img.convert('RGBA')
    # class_activation = class_activation.convert('RGBA')
    # new_img = Image.blend(img, class_activation, 0.55)
    # buffered = BytesIO()
    # new_img.save(buffered, format="PNG")
    # string = base64.b64encode(buffered.getvalue())

    prediction['image'] = string.decode("utf-8")
    return prediction



def base64img_to_results(file):

    classes = ['Normal', 'Pneumonia']
    img = p.image_loader_from_array(file)
    img, class_activation, pred = p.predict_img(img)
    pred = classes[pred.item()]

    name = "results"

    prediction = {
        'name': name,
        'pred': pred,
    }

    mpl.use('Agg')
    plt.ioff()
    plt.axis('off')
    plt.imshow(class_activation, cmap='jet', alpha=1, aspect='equal')
    plt.imshow(img, alpha=0.55, aspect='equal')
    plt.title(name + ' - ' + pred)
    plt.tight_layout()
    fig = plt.gcf()
    # canvas = FigureCanvas(fig)
    # buf = BytesIO()
    fig.savefig('output.png', format='png', bbox_inches='tight')
    # buf.seek(0)
    # print(buf)
    url = 'https://file.io?expires=1w'
    files = {'file': open('output.png', 'rb')}
    r = requests.post(url, files=files)
    if r and r.status_code :
        data = r.json()
        print(data)
        prediction['image'] = data['link']
    # string = base64.b64encode(buf.read())
    # prediction['image'] = "data:image/jpeg;base64," + string.decode("utf-8")
    return prediction

# Create your views here.
def home(request):
    args = {'title': 'Home | CovidScan.ai'}
    return render(request, 'home.html', args)


def team(request):
    args = {'title': 'Team | CovidScan.ai'}
    return render(request, 'team.html', args)


def about(request):
    args = {'title': 'About | CovidScan.ai'}
    return render(request, 'about.html', args)


def live_scan(request):
    args = {'title': 'Live Scan | CovidScan.ai'}
    return render(request, 'scan.html', args)


def demo(request):
    args = {'title': 'Demo | CovidScan.ai'}
    return render(request, 'demo.html', args)

@csrf_exempt
def upload_data(request):
    args = {'title': 'Live Scan | PneumoScan.ai'}
    if request.method == 'POST':
        username = request.POST.get('name')
        email = request.POST.get('email')
        files = request.FILES.getlist('files')
        print(files)
        if username and email and len(files) > 0:
            if not validate_files(files):
                args.update({'message': 'Please upload an appropriate image file',
                             "status": 0})
                return JsonResponse(args)

            images = []
            for i in range(len(files)):
                prediction = img_to_results(files[i])
                images.append(prediction)

            args.update({'message': 'Files are cool',
                         "images": images,
                         "status": 1,
                         "username": username,
                         "email": email})

            # return render(request, 'results.html', args)
            return JsonResponse(args)
        else:
            args.update({'message': 'Please provide all the details',
                         "status": 0})
            return JsonResponse(args)

    else:
        return render(request, 'home.html', args)


@csrf_exempt
def xray_detection(request):
    args = {'title': 'Live X-ray Scan | PneumoScan.ai'}
    if request.method == 'POST':
        body = request.body.decode('utf-8')
        print(type(body))
        img = eval(body)
        img = img['image']
        img = img.split(',')[1]
        img = base64.b64decode(img)
        img = Image.open(BytesIO(img))
        img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
        print(type(img))
        images = []
        prediction = base64img_to_results(img)
        images.append(prediction)

        args.update({'message': 'Files are cool',
                     "images": images,
                     "status": 1
                     })

        # return render(request, 'results.html', args)
        return JsonResponse(args)

    else:
        args.update({'message': 'Bad Request',
                     "status": 0})
        return JsonResponse(args)


@csrf_exempt
def demo_data(request):
    args = {'title': 'Demo | PneumoScan.ai'}
    if request.method == 'POST':
        username = request.POST.get('name')
        email = request.POST.get('email')
        filename = request.POST.get('filename')
        if username and email and filename:

            images = []
            f = filename.split('/')
            filename = f[len(f) - 1]
            file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'img', 'xray-samples', filename)
            prediction = img_to_results(file)
            images.append(prediction)

            args.update({'message': 'Files are cool',
                         "images": images,
                         "status": 1,
                         "username": username,
                         "email": email})

            # return render(request, 'results.html', args)
            return JsonResponse(args)
        else:
            args.update({'message': 'Please provide all the details',
                         "status": 0})
            return JsonResponse(args)

    else:
        return render(request, 'home.html', args)


def ppe_scan(request):
    args = {'title': 'PPE Scan | CovidScan.ai'}
    return render(request, 'ppe.html', args)


def badge_scan(request):
    args = {'title': 'Badge Scan | CovidScan.ai'}
    return render(request, 'badge.html', args)


@csrf_exempt
def badge_decode(request):
    args = {'title': 'Demo | CovidScan.ai'}
    if request.method == 'POST':
        body = request.body.decode('utf-8')
        print(type(body))
        img = eval(body)
        img = img['image']
        # print(img)
        decoded = barCode.decode(img)

        if decoded:
            json_data = json.dumps(decoded)
            args.update({
                'data': json_data,
                'status': 1
            })
            print(args)
            return JsonResponse(args, safe=False)

        else:
            args.update({
                'data': 'No barcode found',
                'status': 0
            })
            return JsonResponse(args)
    else:
        return render(request, 'home.html', {})

@csrf_exempt
def predict_ppe(request):
    if request.method == 'POST':
        body = request.body.decode('utf-8')
        print(type(body))
        img = eval(body)
        img = img['image']
        img = img.split(',')[1]
        img = base64.b64decode(img)
        img = Image.open(BytesIO(img))
        img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)

        prediction = infer.inference(img, draw_result=False, show_result=False, target_shape=(260, 260))
        p = prediction[0]
        if p[1] > 0.9 and p[0] == 0:
            p[0] = 0
        else:
            p[0] = 1

        pred = {'class': str(p[0]), 'score': str(p[1]), 'xmin': str(p[2]), 'ymin': str(p[3]), 'xmax': str(p[4]),
                'ymax': str(p[5])}
        args = {'data': pred, 'status': 1}
        return JsonResponse(args)
    else:
        return render(request, 'home.html', {})
