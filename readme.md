# CovidScan: An AI Tool For PPE Check & COVID19 Testing

PneumoScan.ai is developed to be a secured AI tool with the purpose to assist radiologists with COVID-19 dectection on chest X-ray, and medical staff’s PPE safety check amid this COVID-19 pandemics.


## Contents

1. [Short description](#short-description)
1. [Demo video](#demo-video)
1. [The architecture](#the-architecture)
1. [Long description](#long-description)
1. [Project roadmap](#project-roadmap)
1. [Getting started](#getting-started)
1. [Running the tests](#running-the-tests)
1. [Built with](#built-with)
1. [Authors](#authors)

## Short description

### What's the problem?

Using the power of deep learning image recognition models, CovidScan.ai is created as a full-scaled AI tool for radiology clinics and hospitals. It can automate the process of security log-in, PPE safety check for medical staff and assist radiologists to determine the sign of COVID-19 on chest X-ray images with high accuracy indicate pneumonia. This tool of cutting edge technology can be used to reduce the workload for clinicians, and speed up patients’ wait time for pneumonia lab results in this critical time of the COVID-19 pandemic. 
CovidScan.ai is a full-scaled AI tool for radiology clinics and hospitals to cut down patients' wait-time during COVID19 pandemics.

## Demo video

[![Watch the video](https://i.ibb.co/Q6j7Q1J/Screen-Shot-2020-04-28-at-1-50-21-AM.png)](https://youtu.be/uRWYUf5drx4)

## The architecture

![Video transcription/translation app](https://i.ibb.co/GQ4HT0X/covidscan-architecture.png)


## Long description

[More detail is available here](description.md)

## Project roadmap

##### Our future plan for the project is as followings:
* We will work on improving the performance of the platform, preferably by reading more scientific literature on state-of-art deep learning models implemented for radiology.
* We also plan to add the bound box around the suspected area of infection on top of the heatmap to make the output image more interpretable for the radiologists.
* In many pieces of literature, they mentioned developing the NLP model on radiology report with other structured variables such as age, race, gender, temperature... and integrating it with the computer vision model of chest X-ray to give the expert radiologist’s level of diagnosis. (Irvin et al., 2019; Mauro et al., 2019) We may try to implement that as we move further with the project in the future.
* With the improved results, we will publish these findings and methodologies in a user-interface journal so that it can be reviewed by expert computer scientists and radiologists in the field.
* Eventually, we will expand our classes to include more pneumonia-related diseases such as atelectasis, cardiomegaly, effusion, infiltration, etc. so that this platform can be widely used by the radiologists for general diagnosis even after the COVID-19 pandemics is over. Our end goal is to make this tool a scalable that can be used in all the radiology clinic across the globe, even in the rural area with limited access to the internet like those in Southeast Asia or Africa.


## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing
Now Let's Install Virtualenv with pip. There are other ways also to install virtualenv but I prefer this.*

```sh
$ easy_install pip
```
Next step is to install the virtualenv package: 

```sh
$ pip install virtualenv
```

Great you have installed ```virtualenv```on your machine.

Cloning the project. Type this command in terminal to clone the repo.
```sh
$ git clone https://gitlab.com/sagban/pneumoscan-ppe
```

Create an Environment with virtualenv
```sh
$ cd covidscan
```
To create the environment with virtualenv:
```sh
$ virtualenv venv  #see alternative if you are using other than LINUX/UNIX.
```
 After creating virtual environment, it's time to activate it. Type this command
```sh
$ source venv/bin/activate
```

To check wether the cloning process done corectly type ``` ls```,and it'll look like this.
```sh
ls
app     manage.py   pneomonia    requirements.txt    venv
``` 

```sh
$ pip install -r requirements.txt
```
Now, start the deployment server
```sh
$ python manage.py runserver
```
If everything worked fine >>
Congratulations, you setup the covidscan project in your pc.


## Running the tests

Open the link `http://127.0.0.1:8000` in your browser to test the project. 

## Live demo
Currently, we are working on the deployment of the project on the IBM cloud foundry python instance.

## Built with

* Torch (torch.nn, torch.optim, torchvision, torchvision.transforms)
* Django
* Numpy
* Matplotlib
* Scipy
* PIL
* Tensorflow
* JQuery
* IBM Cloud Foundry

## Authors
* [Vi Ly](https://www.linkedin.com/in/vi-k-ly/)
* [Sagar Bansal](https://www.linkedin.com/in/sagar-bansal-448097140/)
* [Nihal](https://www.linkedin.com/in/sagar-bansal-448097140/)
![Video transcription/translation app](https://i.ibb.co/Wfymfhs/Screen-Shot-2020-04-28-at-1-49-48-AM.png)


